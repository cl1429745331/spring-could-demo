package com.example.mallprotal.controller;

import com.example.api.IOrderService;
import com.example.api.IPromotionService;
import com.example.goodsserviceapi.IGoodsService;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixProperty;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.ServiceInstance;
import org.springframework.cloud.client.loadbalancer.LoadBalancerClient;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import java.util.Random;

/**
 * @author 陳樂
 * @version 1.0.0
 * @ClassName OrderController.java
 * @Description TODO
 * @createTime 2022年06月12日 20:00:00
 */
@RestController
@RequestMapping("/order")
@Slf4j
public class OrderController {

    @Autowired
    private RestTemplate restTemplate;

    @Autowired
    private LoadBalancerClient loadBalancerClient;

    // 代理对象
    @Autowired
    private IGoodsService goodsService;

    @Autowired
    private IPromotionService promotionService;

    @Autowired
    private IOrderService orderService;

    private String getGoodsServer() {
        String serverList = "http://localhost:9090/goods,http://localhost:9093/goods";
        String servers[] = serverList.split(",");
        Random random = new Random();
        return servers[random.nextInt(servers.length)];
    }

    @GetMapping()
    public String order() {
        log.info("begin do order");
//        ServiceInstance choose = loadBalancerClient.choose("goods-service");
//        String url = String.format("http://%s:%s", choose.getHost(), choose.getPort());
//        log.info("ribbon-url" + url);

//        使用openfiegn
        String goods = goodsService.getGoodsById();
        String promotion = promotionService.getPromotionById();
        String result = orderService.createOrder(goods, promotion);
        return result;

//        未使用openfiegn
//        String url = "http://goods-service/goods";
//        String goodsInfo = restTemplate.getForObject(url, String.class);
//        String promotionInfo = restTemplate.getForObject("http://marking-service/promotion", String.class);
//
//        MultiValueMap<String, Object> param = new LinkedMultiValueMap<>();
//        param.add("goodsInfo", goodsInfo);
//        param.add("promotionInfo", promotionInfo);
//        HttpEntity<MultiValueMap<String, Object>> httpEntity = new HttpEntity<>(param, new HttpHeaders());
//        ResponseEntity<String> response = restTemplate.postForEntity("http://order-service/order", httpEntity, String.class);
//        return response.getBody();
    }

    //HystrixCommandProperties
    @HystrixCommand(commandProperties = {
            @HystrixProperty(name = "circuitBreaker.requestVolumeThreshold", value = "2"), //请求阈值
            @HystrixProperty(name = "circuitBreaker.sleepWindowInMilliseconds", value = "5000"),//熔断窗口持续时间
            @HystrixProperty(name = "circuitBreaker.errorThresholdPercentage", value = "50")//错误比
    }, fallbackMethod = "fallback")
    @GetMapping("/hystrix/{num}")
    public String hystrix(@PathVariable("num") int num) {
        if (num % 2 == 0) {
            return "正常访问。";
        }
        log.info("begin do order");
        String goods = goodsService.getGoodsById();
        String promotion = promotionService.getPromotionById();
        String result = orderService.createOrder(goods, promotion);
        return result;
    }

    public String fallback(int num) {
        return "触发降级";
    }
}
