package com.example.goodsservice.controller;

import com.example.goodsserviceapi.IGoodsService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author 陳樂
 * @version 1.0.0
 * @ClassName GoodsService.java
 * @Description TODO
 * @createTime 2022年06月13日 21:58:00
 */
@Slf4j
@RestController
public class GoodsService implements IGoodsService  {

    @Value("${server.port}")
    private String port;

    /**
     * 根据ID查询商品信息
     *
     * @return
     */
    @GetMapping("/goods")
    public String getGoodsById() {

        try {
            Thread.sleep(5000);
        } catch (Exception e){
            e.printStackTrace();
        }
        log.info("收到请求，端口为：{}", port);
        return "返回商品信息";
    }
}

