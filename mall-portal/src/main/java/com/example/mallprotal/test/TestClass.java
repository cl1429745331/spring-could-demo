package com.example.mallprotal.test;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * @author 陳樂
 * @version 1.0.0
 * @ClassName TestClass.java
 * @Description TODO
 * @createTime 2022年10月14日 23:24:00
 */
@AllArgsConstructor
@Data
public class TestClass {
    private String name;
}