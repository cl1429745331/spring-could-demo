package com.example.fallback;

import com.example.feignclient.IGoodsServiceFeignClient;
import org.springframework.stereotype.Component;

/**
 * @author 陳樂
 * @version 1.0.0
 * @ClassName IGoodsServiceFallback.java
 * @Description TODO
 * @createTime 2022年07月03日 22:42:00
 */
//@Component
public class IGoodsServiceFallback implements IGoodsServiceFeignClient {

    @Override
    public String getGoodsById() {
        return "查询商品信息异常，触发降级保护机制。";
    }
}
