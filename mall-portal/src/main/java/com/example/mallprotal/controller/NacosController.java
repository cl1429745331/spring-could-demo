package com.example.mallprotal.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author 陳樂
 * @version 1.0.0
 * @ClassName NacosController.java
 * @Description TODO
 * @createTime 2022年06月26日 22:37:00
 */
@RestController
@RefreshScope
public class NacosController {

    @Value("${txt:false}")
    private String txt;

    @GetMapping("/txt")
    public String get(){
        return txt;
    }
}
