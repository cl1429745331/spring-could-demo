package com.example.goodsservice.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author 陳樂
 * @version 1.0.0
 * @ClassName HealthController.java
 * @Description TODO
 * @createTime 2022年06月15日 22:31:00
 */
@RestController
@Slf4j
@Deprecated
public class HealthController {

    @GetMapping("/healthCheck")
    public String health(){
        log.info("healthCheck");
        return "SUCCESS";
    }

}
