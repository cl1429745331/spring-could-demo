package com.example.mallprotal.config;

import com.netflix.loadbalancer.IPing;
import com.netflix.loadbalancer.Server;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.impl.client.DefaultHttpClient;

import java.io.IOException;

/**
 * @author 陳樂
 * @version 1.0.0
 * @ClassName HealthChecker.java
 * @Description TODO
 * @createTime 2022年06月15日 22:37:00
 */
public class HealthChecker implements IPing {
    @Override
    public boolean isAlive(Server server) {
        //服务的名称
        String url = "http://" + server.getId() + "/healthCheck";
        boolean isAlive = true;
        HttpClient httpClient = new DefaultHttpClient();
        HttpUriRequest request = new HttpGet(url);
        try {
            HttpResponse response = httpClient.execute(request);
            isAlive = response.getStatusLine().getStatusCode() == 200;
        } catch (IOException e) {
            e.printStackTrace();
            isAlive = false;
        }
        return isAlive;
    }
}
