package com.example.api;

import org.springframework.web.bind.annotation.GetMapping;

/**
 * @author 陳樂
 * @version 1.0.0
 * @ClassName IPromotionService.java
 * @Description TODO
 * @createTime 2022年06月21日 22:38:00
 */
public interface IPromotionService {

    @GetMapping("/promotion")
    String getPromotionById();
}
