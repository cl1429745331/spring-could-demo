package com.example.markingservice.controller;

import com.example.api.IPromotionService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author 陳樂
 * @version 1.0.0
 * @ClassName PromotionService.java
 * @Description TODO
 * @createTime 2022年06月13日 22:01:00
 */
@RestController
public class PromotionService implements IPromotionService {

    @GetMapping("/promotion")
    public String getPromotionById() {
        return "查询到指定商品的促销信息";
    }
}
