package com.example.goodsserviceapi;

import org.springframework.web.bind.annotation.GetMapping;

/**
 * @author 陳樂
 * @version 1.0.0
 * @ClassName IGoodsService.java
 * @Description TODO
 * @createTime 2022年06月21日 22:33:00
 */
public interface IGoodsService {

    @GetMapping("/goods")
    String getGoodsById();

}
