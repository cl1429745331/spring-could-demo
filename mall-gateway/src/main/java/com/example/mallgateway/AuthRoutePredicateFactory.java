package com.example.mallgateway;

import org.springframework.cloud.gateway.handler.predicate.AbstractRoutePredicateFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ServerWebExchange;

import java.util.Arrays;
import java.util.List;
import java.util.function.Predicate;

/**
 * @author 陳樂
 * @version 1.0.0
 * @ClassName AuthRoutePredicateFactory.java
 * @Description 根据Header中，带authorization标记将其拦截进行路由
 * @createTime 2022年09月27日 23:06:00
 */
@Component
public class AuthRoutePredicateFactory extends AbstractRoutePredicateFactory<AuthRoutePredicateFactory.Config> {

    public static final String NAME_KEY="name";

    public AuthRoutePredicateFactory() {
        super(Config.class);
    }

    //根据顺序获取属性值
    @Override
    public List<String> shortcutFieldOrder() {
        return Arrays.asList(NAME_KEY);
    }

    @Override
    public Predicate<ServerWebExchange> apply(Config config) {
        return exchange -> {
            HttpHeaders headers = exchange.getResponse().getHeaders();
            List<String> header = headers.get(config.getName());
            return header!=null && header.size()>0;
        };
    }

    public static class Config {

        private String name;

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

    }

}
