package com.example.feignclient;

import com.example.api.IOrderService;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * @author 陳樂
 * @version 1.0.0
 * @ClassName IOrderServiceFeignClient.java
 * @Description TODO
 * @createTime 2022年06月21日 22:59:00
 */
@FeignClient(name = "order-service")
public interface IOrderServiceFeignClient extends IOrderService {

}
