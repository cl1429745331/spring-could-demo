package com.example.feignclient;

import feign.Logger;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author 陳樂
 * @version 1.0.0
 * @ClassName FeignClientLogConfiguration.java
 * @Description TODO
 * @createTime 2022年06月22日 22:58:00
 */
@Configuration
public class FeignClientLogConfiguration {

    /**
     * NONE
     * BASIC
     * HEAD
     * FULL
     * @return
     */
    @Bean
    Logger.Level feignLogger(){
        return Logger.Level.FULL;
    }
}
