package com.example.mallprotal;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.circuitbreaker.EnableCircuitBreaker;
import org.springframework.cloud.openfeign.EnableFeignClients;

@EnableCircuitBreaker //开启熔断
@EnableFeignClients(basePackages = "com.example.feignclient")
@SpringBootApplication
public class MallProtalApplication {

    public static void main(String[] args) {
        SpringApplication.run(MallProtalApplication.class, args);
    }

}
