package com.example.mallprotal.test;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author 陳樂
 * @version 1.0.0
 * @ClassName TestConfig.java
 * @Description TODO
 * @createTime 2022年10月14日 23:24:00
 */
@Configuration
public class TestConfig {

    @Bean("testClass1")
    @Qualifier
    TestClass testClass(){
        return new TestClass("testClass1");
    }

    @Bean("testClass2")
    TestClass testClass2(){
        return new TestClass("testClass2");
    }

}
