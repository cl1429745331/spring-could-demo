package com.example.orderservice.controller;

import com.example.api.IOrderService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author 陳樂
 * @version 1.0.0
 * @ClassName OrderService.java
 * @Description TODO
 * @createTime 2022年06月13日 22:02:00
 */
@Slf4j
@RestController
public class OrderService implements IOrderService {

    @PostMapping("/order")
    public String createOrder(@RequestParam String goodsInfo, @RequestParam String promotionInfo) {
        log.info("开始创建订单,请求参数,{},{}", goodsInfo, promotionInfo);
        return "订单创建成功";
    }
}

