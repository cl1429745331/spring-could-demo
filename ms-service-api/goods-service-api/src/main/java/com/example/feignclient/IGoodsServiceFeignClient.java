package com.example.feignclient;

import com.example.fallback.IGoodsServiceFallback;
import com.example.goodsserviceapi.IGoodsService;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * @author 陳樂
 * @version 1.0.0
 * @ClassName IGoodsServiceFeignClient.java
 * @Description TODO
 * @createTime 2022年06月21日 22:56:00
 */
@FeignClient(name = "goods-service",
        configuration = FeignClientLogConfiguration.class,
        fallback = IGoodsServiceFallback.class)
public interface IGoodsServiceFeignClient extends IGoodsService {


}
