package com.example.mallprotal.config;

import com.example.fallback.IGoodsServiceFallback;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author 陳樂
 * @version 1.0.0
 * @ClassName HystrixFallbackConfiguration.java
 * @Description TODO
 * @createTime 2022年07月03日 23:00:00
 */
@Configuration
public class HystrixFallbackConfiguration {

    @Bean
    public IGoodsServiceFallback goodsServiceFallback() {
        return new IGoodsServiceFallback();
    }

}
