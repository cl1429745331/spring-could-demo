package com.example.springbootnacos;

import com.alibaba.nacos.api.config.annotation.NacosValue;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author 陳樂
 * @version 1.0.0
 * @ClassName NacosConfigController.java
 * @Description TODO
 * @createTime 2022年06月26日 22:16:00
 */
@RestController
public class NacosConfigController {


    @NacosValue(value = "${txt:我是默认值}", autoRefreshed = true)
    private String txt;


    @GetMapping("/txt")
    public String get(){
        return txt;
    }
}
