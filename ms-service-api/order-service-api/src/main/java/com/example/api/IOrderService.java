package com.example.api;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * @author 陳樂
 * @version 1.0.0
 * @ClassName IOrderService.java
 * @Description TODO
 * @createTime 2022年06月21日 22:40:00
 */
public interface IOrderService {

    //记得在RequestParam注解加上参数名，否则会报错
    @PostMapping("/order")
    String createOrder(@RequestParam("goodsInfo") String goodsInfo, @RequestParam("promotionInfo") String promotionInfo);

}
